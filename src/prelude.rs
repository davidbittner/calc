use std::collections::HashMap;
use std::sync::Arc;

pub use crate::expressions::operand::Operand;
pub use crate::expressions::operand::*;
pub use crate::expressions::*;
pub use crate::{ExpressionBuilder, RootExpression};

pub type MathFunc = fn(Vec<Operand>) -> Operand;
pub type FuncDict = HashMap<String, Arc<MathFunc>>;

lazy_static::lazy_static! {
    pub static ref STD_LIB: FuncDict = {
        let mut dict = FuncDict::new();
        dict.insert("sin".to_string(), Arc::new(|operand| {
            match operand.first().unwrap() {
                Operand::Rational(rat) => {
                    let float: f64 = (*rat).into();
                    Operand::Irrational(float.sin().into())
                },
                Operand::Irrational(irrat) => {
                    Operand::Irrational(irrat.sin().into())
                }
            }
        }));

        dict.insert("cos".to_string(), Arc::new(|operand| {
            match operand.first().unwrap() {
                Operand::Rational(rat) => {
                    let float: f64 = (*rat).into();
                    Operand::Irrational(float.cos().into())
                },
                Operand::Irrational(irrat) => {
                    Operand::Irrational(irrat.cos().into())
                }
            }
        }));

        dict.insert("tan".to_string(), Arc::new(|operand| {
            match operand.first().unwrap() {
                Operand::Rational(rat) => {
                    let float: f64 = (*rat).into();
                    Operand::Irrational(float.tan().into())
                },
                Operand::Irrational(irrat) => {
                    Operand::Irrational(irrat.tan().into())
                }
            }
        }));

        dict.insert("e".to_string(), Arc::new(|_| {
            std::f64::consts::E.into()
        }));

        dict.insert("pi".to_string(), Arc::new(|_| {
            std::f64::consts::PI.into()
        }));

        dict
    };
}

pub(in crate) trait Evaluatable {
    fn eval(&self, fd: &FuncDict) -> Operand;
}

pub(in crate) trait ParseExpr: Sized {
    type Err;

    fn parse(s: &str, fd: &FuncDict) -> Result<Expression, Self::Err>;
}
