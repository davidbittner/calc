use super::*;
use std::fmt::{Display, Formatter, Result as FmtResult};
use thiserror::Error;

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Irrational(pub f64);

#[derive(Debug, PartialEq, Eq, Error)]
pub enum IrrationalError {
    #[error("error, '{0}' is not a valid f64 (decimal number)")]
    ParseError(String),
}

impl Display for Irrational {
    fn fmt(&self, fmt: &mut Formatter) -> FmtResult {
        write!(fmt, "{}", self.0)
    }
}

impl PartialEq<f64> for Irrational {
    fn eq(&self, oth: &f64) -> bool {
        (self.deref()) == oth
    }
}

impl PartialEq<Irrational> for f64 {
    fn eq(&self, oth: &Irrational) -> bool {
        self == (oth.deref())
    }
}

impl ParseExpr for Irrational {
    type Err = IrrationalError;
    fn parse(s: &str, _: &FuncDict) -> std::result::Result<Expression, Self::Err> {
        let num: f64 = s
            .parse()
            .map_err(|_| IrrationalError::ParseError(s.to_owned()))?;

        Ok(Expression::Operand(Irrational(num).into()))
    }
}

impl From<f64> for Irrational {
    fn from(f: f64) -> Self {
        Irrational(f)
    }
}

impl From<Irrational> for Operand {
    fn from(i: Irrational) -> Self {
        Self::Irrational(i)
    }
}

impl Add<Rational> for Irrational {
    type Output = Irrational;
    fn add(self, oth: Rational) -> Self::Output {
        let a: f64 = oth.into();
        Irrational(*self + a)
    }
}

impl Sub<Rational> for Irrational {
    type Output = Irrational;
    fn sub(self, oth: Rational) -> Self::Output {
        let a: f64 = oth.into();
        Irrational(*self - a)
    }
}

impl Mul<Rational> for Irrational {
    type Output = Irrational;
    fn mul(self, oth: Rational) -> Self::Output {
        let a: f64 = oth.into();
        Irrational(*self * a)
    }
}

impl Div<Rational> for Irrational {
    type Output = Irrational;
    fn div(self, oth: Rational) -> Self::Output {
        let a: f64 = oth.into();
        Irrational(*self / a)
    }
}

impl Rem<Rational> for Irrational {
    type Output = Irrational;
    fn rem(self, oth: Rational) -> Self::Output {
        let a: f64 = oth.into();
        Irrational(*self % a)
    }
}

impl Rem<Irrational> for Irrational {
    type Output = Irrational;
    fn rem(self, oth: Irrational) -> Self::Output {
        Irrational(*self % *oth)
    }
}

impl Deref for Irrational {
    type Target = f64;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn test_parse() {
        use super::*;
        let fd = FuncDict::default();

        let num: Operand = Irrational::parse("-126.7", &fd).unwrap().eval(&fd);
        match num {
            Operand::Irrational(ir) => {
                assert_eq!(-126.7, ir);
            }
            _ => panic!("should be irrational, but is rational!"),
        }

        let num: Operand = Irrational::parse("15.0001", &fd).unwrap().eval(&fd);
        match num {
            Operand::Irrational(ir) => {
                assert_eq!(15.0001, ir);
            }
            _ => panic!("should be irrational, but is rational!"),
        }
    }
}
