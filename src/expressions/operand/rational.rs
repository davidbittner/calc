use super::*;

#[derive(Default, PartialEq, Eq, Debug, Clone, Copy)]
pub struct Rational {
    pub(crate) top: i64,
    pub(crate) bot: i64,
}

#[derive(Debug, PartialEq, Eq, Error)]
pub enum RationalError {
    #[error("error, '{0}' is not a valid rational number")]
    IntError(String),
    #[error("error, failed to parse number with radix: '{0}'")]
    RadixError(String),
}

use std::fmt::{Display, Formatter, Result as FmtResult};
impl Display for Rational {
    fn fmt(&self, fmt: &mut Formatter) -> FmtResult {
        if self.bot == 1 {
            write!(fmt, "{}", self.top)
        } else {
            write!(fmt, "{}/{}", self.top, self.bot)
        }
    }
}

impl From<i64> for Rational {
    fn from(oth: i64) -> Self {
        Self { top: oth, bot: 1 }
    }
}

impl Rational {
    pub fn new(top: i64, bot: i64) -> Self {
        Rational { top, bot }
    }

    pub fn abs(&mut self) {
        self.top = self.top.abs();
        self.bot = self.bot.abs();
    }

    pub fn reciprocate(&mut self) {
        std::mem::swap(&mut self.top, &mut self.bot);
    }

    pub fn simplify(mut self) -> Self {
        if self.top == 0 {
            self.bot = if self.bot > 0 { 1 } else { 0 };
        } else if self.top < 0 && self.bot < 0 {
            self.top = self.top.abs();
            self.bot = self.bot.abs();
        } else if self.bot < 0 {
            self.bot = self.bot.abs();
            self.top = -self.top;
        }

        if (self.top % self.bot) == 0 && self.bot != 1 {
            self.top /= self.bot;
            self.bot = 1;
        } else if self.top == self.bot {
            self.top = 1;
            self.bot = 1;
        } else {
            use std::cmp::min;

            let low: i64 = min(self.top.abs(), self.bot.abs());
            let mut facs: Vec<_> = (2..=low)
                .filter(|x| (self.bot % x) == 0 && (self.top % x) == 0)
                .collect();

            if let Some(fac) = facs.pop() {
                self.top /= fac;
                self.bot /= fac;
            }
        }

        self
    }
}

use std::cmp::{Ordering, PartialOrd};
impl PartialOrd for Rational {
    fn partial_cmp(&self, oth: &Self) -> Option<Ordering> {
        if self.bot == oth.bot || oth.bot == 0 {
            Some(self.top.cmp(&oth.top))
        } else {
            let a_out = self.top / self.bot;
            let b_out = oth.top / oth.bot;

            let cmp = a_out.cmp(&b_out);
            match cmp {
                Ordering::Equal => {
                    let a = self.top - (self.bot * a_out);
                    let b = oth.top - (oth.bot * b_out);

                    Some(a.cmp(&b))
                }
                _ => Some(cmp),
            }
        }
    }
}

impl From<Rational> for f64 {
    fn from(rat: Rational) -> Self {
        rat.top as f64 / rat.bot as f64
    }
}

impl ParseExpr for Rational {
    type Err = RationalError;
    fn parse(s: &str, _: &FuncDict) -> std::result::Result<Expression, RationalError> {
        let s = s.trim();
        let ret = if let Some(frac_pos) = s.find('/') {
            let top = &s[..frac_pos].trim();
            let bot = &s[frac_pos + 1..].trim();

            let top: i64 = if top.contains('_') {
                parse_num_wradix(top)?
            } else {
                top.parse()
                    .map_err(|_| RationalError::IntError(s.to_owned()))?
            };

            let bot: i64 = if bot.contains('_') {
                parse_num_wradix(bot)?
            } else {
                bot.parse()
                    .map_err(|_| RationalError::IntError(bot.to_string()))?
            };

            Rational::new(top, bot)
        } else if s.contains('_') {
            let num: i64 = parse_num_wradix(s)?;
            Rational::new(num, 1)
        } else {
            let num: i64 = s
                .parse()
                .map_err(|_| RationalError::IntError(s.to_string()))?;

            Rational::new(num, 1)
        }
        .simplify();
        Ok(Expression::Operand(ret.into()))
    }
}

fn parse_num_wradix(s: &str) -> std::result::Result<i64, RationalError> {
    let nums: Vec<_> = s.splitn(2, '_').collect();

    let (num, radix) = (
        nums[0],
        nums[1]
            .parse()
            .map_err(|_| RationalError::RadixError(nums[0].into()))?,
    );

    Ok(i64::from_str_radix(num, radix).map_err(|_| RationalError::IntError(nums[0].into()))?)
}

impl From<Rational> for Operand {
    fn from(rat: Rational) -> Self {
        use Operand::*;
        Rational(rat)
    }
}

impl Add<Rational> for Rational {
    type Output = Rational;
    fn add(self, oth: Self) -> Self::Output {
        if self.bot == 0 && self.top == 0 {
            oth
        } else if oth.bot == 0 && oth.top == 0 {
            self
        } else if self.bot == oth.bot {
            Rational {
                top: self.top + oth.top,
                bot: self.bot,
            }
        } else {
            let new_bottom = self.bot * oth.bot;
            Rational {
                top: (self.top * oth.bot) + (oth.top * self.bot),
                bot: new_bottom,
            }
        }
        .simplify()
    }
}

impl Sub<Rational> for Rational {
    type Output = Rational;
    fn sub(self, oth: Self) -> Self::Output {
        if self.bot == oth.bot {
            Rational {
                top: self.top - oth.top,
                bot: self.bot,
            }
        } else {
            let new_bottom = self.bot * oth.bot;
            Rational {
                top: (self.top * oth.bot) - (oth.top * self.bot),
                bot: new_bottom,
            }
        }
        .simplify()
    }
}

impl Sub<Irrational> for Rational {
    type Output = Irrational;
    fn sub(self, oth: Irrational) -> Self::Output {
        let a: f64 = self.into();
        Irrational(a - *oth)
    }
}

impl Add<Irrational> for Rational {
    type Output = Irrational;
    fn add(self, oth: Irrational) -> Self::Output {
        let a: f64 = (self.top as f64) / (self.bot as f64);
        Irrational(oth.0 + a)
    }
}

impl Mul<Rational> for Rational {
    type Output = Rational;
    fn mul(self, oth: Rational) -> Self::Output {
        Rational {
            top: self.top * oth.top,
            bot: self.bot * oth.bot,
        }
        .simplify()
    }
}

impl Mul<Irrational> for Rational {
    type Output = Irrational;
    fn mul(self, oth: Irrational) -> Self::Output {
        let val = self.top as f64 / self.bot as f64;
        (val * *oth).into()
    }
}

impl Div<Rational> for Rational {
    type Output = Rational;
    fn div(self, oth: Rational) -> Self::Output {
        Rational {
            top: self.top * oth.bot,
            bot: self.bot * oth.top,
        }
        .simplify()
    }
}

impl Div<Irrational> for Rational {
    type Output = Irrational;
    fn div(self, oth: Irrational) -> Self::Output {
        let val = self.top as f64 / self.bot as f64;
        (val / *oth).into()
    }
}

impl Rem<Rational> for Rational {
    type Output = Rational;
    fn rem(self, _: Rational) -> Self::Output {
        //TODO implement!
        unimplemented!()
    }
}

impl Rem<Irrational> for Rational {
    type Output = Irrational;
    fn rem(self, oth: Irrational) -> Self::Output {
        let a: f64 = self.into();
        Irrational(a % *oth)
    }
}

#[cfg(test)]
mod rational_tests {
    use super::*;

    #[test]
    fn parse() {
        let dict = FuncDict::new();

        let p = "1523 / 4";
        let num = Rational::parse(p, &dict);
        assert_eq!(
            Ok(Expression::Operand(Operand::Rational(Rational::new(
                1523, 4
            )))),
            num
        );
    }

    #[test]
    fn parse_w_radix() {
        let p = "AA_16";
        let num = parse_num_wradix(p).unwrap();
        assert_eq!(170, num);

        let p = "111011_2";
        let num = parse_num_wradix(p).unwrap();
        assert_eq!(59, num);
    }

    #[test]
    fn simplify() {
        let rat = Rational::new(27, 9).simplify();
        assert_eq!(Rational::new(3, 1), rat);

        let rat = Rational::new(90, 90).simplify();
        assert_eq!(Rational::new(1, 1), rat);

        let rat = Rational::new(23, 97).simplify();
        assert_eq!(Rational::new(23, 97), rat);
    }

    #[test]
    fn add() {
        let a = Rational::new(15, 2);
        let b = Rational::new(22, 7);
        assert_eq!(Rational::new(149, 14), a + b);

        let a = Rational::new(42, 7);
        let b = Rational::new(276, 98);
        assert_eq!(Rational::new(432, 49), a + b);

        let a = Rational::new(15, -2);
        let b = Rational::new(22, 3);
        assert_eq!(Rational::new(-1, 6), a + b);
    }

    #[test]
    fn div() {
        let a = Rational::new(10, 7);
        let b = Rational::new(7, 10);
        assert_eq!(Rational::new(100, 49), a / b);

        let a = Rational::new(10, 7);
        let b = Rational::new(10, 7);
        assert_eq!(Rational::new(1, 1), a / b);
    }
}
