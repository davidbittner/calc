use super::*;
use std::sync::Arc;

///A type of expression that invokes a function call
///such as sine or cosine.
#[derive(Clone, PartialEq, Debug)]
pub struct FuncExp {
    pub func_name: String,
    pub func: Arc<MathFunc>,
    pub params: Vec<Expression>,
}

use std::fmt::{Display, Formatter, Result as FmtResult};
impl Display for FuncExp {
    fn fmt(&self, fmt: &mut Formatter) -> FmtResult {
        use std::fmt::Write;

        write!(fmt, "{}(", self.func_name)?;
        let mut buff = String::new();

        for param in self.params.iter() {
            write!(buff, "{},", param)?;
        }

        buff.pop();
        write!(fmt, "{})", buff)
    }
}

#[derive(Debug, PartialEq, Eq, Error)]
pub enum FuncParseError {
    #[error("could not generate a useful error")]
    Garbage,
    #[error("unknown function '{0}'")]
    NotAFunction(String),
    #[error("failed to parse expression inside function call '{func_name}':\n\t\t'{err}'")]
    ExpressionError {
        err: Box<ExpressionParseError>,
        func_name: String,
    },
    #[error("'{given}' are invalid parameters for function '{name}', wants {needs} parameters, but was given {has}")]
    InvalidParameterCount {
        name: String,
        given: String,
        needs: u32,
        has: u32,
    },
    #[error("no matching parentheses found for function '{0}'")]
    NoMatchingParen(String),
}

impl IsErrorUseful for FuncParseError {
    fn is_useful(&self) -> bool {
        !matches!(self, Self::Garbage)
    }
}

impl From<FuncExp> for Expression {
    fn from(fe: FuncExp) -> Self {
        Expression::Function(fe)
    }
}

fn walk_and_split(s: &str) -> Vec<&str> {
    let mut ret = Vec::new();
    let mut last = 0;

    for (ind, _) in s.match_indices(',') {
        let left_paren = s[last..ind].matches('(').count();

        let right_paren = s[last..ind].matches(')').count();

        if left_paren == right_paren {
            ret.push(s[last..ind].trim());
            last = ind + 1;
            continue;
        }
    }

    if !s[last..s.len()].is_empty() {
        ret.push(s[last..s.len()].trim());
    }

    ret
}

pub(crate) fn find_matching(s: &str, start: usize) -> Option<usize> {
    let mut counter = 0;
    let (up, down) = match s.chars().nth(start)? {
        '(' => ('(', ')'),
        '{' => ('{', '}'),
        '[' => ('[', ']'),
        _ => return None,
    };

    for (ind, ch) in s.chars().enumerate().skip(start) {
        if ch == up {
            counter += 1;
        } else if ch == down {
            counter -= 1;
        }

        if counter == 0 {
            return Some(ind);
        }
    }

    None
}

impl ParseExpr for FuncExp {
    type Err = FuncParseError;
    fn parse(s: &str, fd: &FuncDict) -> Result<Expression, Self::Err> {
        let s = s.trim();
        if let Some(paren) = s.find('(') {
            let func_name = &s[..paren];

            let matching = find_matching(s, paren)
                .ok_or_else(|| FuncParseError::NoMatchingParen(s[..paren].to_string()))?;

            let parens = &s[paren + 1..matching];

            let mut params = Vec::new();
            for param in walk_and_split(parens).into_iter() {
                params.push(Expression::parse(param, fd).map_err(|err| {
                    FuncParseError::ExpressionError {
                        err: Box::new(err),
                        func_name: func_name.to_owned(),
                    }
                })?);
            }

            let func = fd
                .get(func_name)
                .ok_or_else(|| FuncParseError::NotAFunction(func_name.to_string()))?;

            Ok(Self {
                func_name: String::from(func_name),
                func: Arc::clone(func),
                params,
            }
            .into())
        } else {
            Err(FuncParseError::Garbage)
        }
    }
}

impl Evaluatable for FuncExp {
    fn eval(&self, fd: &FuncDict) -> Operand {
        let params: Vec<_> = self.params.iter().map(|expr| expr.eval(fd)).collect();

        (self.func)(params)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_walk_and_split() {
        let params = "5, abc(1, 2, b(3, 4, 5)), 10101_2";
        let ret = walk_and_split(params);

        assert_eq!(3, ret.len());
        assert_eq!(&["5", "abc(1, 2, b(3, 4, 5))", "10101_2"], ret.as_slice());

        let params = "abc(1+2)/1";
        let ret = walk_and_split(params);

        assert_eq!(1, ret.len());
        assert_eq!(&["abc(1+2)/1"], ret.as_slice());
    }
}
