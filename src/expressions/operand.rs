use std::fmt::{Display, Formatter, Result as FmtResult};
use std::ops::*;

use super::*;
use crate::prelude::*;

pub mod irrational;
pub mod rational;
pub use irrational::*;
pub use rational::*;
use thiserror::Error;

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Operand {
    Rational(Rational),
    Irrational(Irrational),
}

impl Display for Operand {
    fn fmt(&self, fmt: &mut Formatter) -> FmtResult {
        match self {
            Operand::Rational(r) => write!(fmt, "{}", r),
            Operand::Irrational(i) => write!(fmt, "{}", i),
        }
    }
}

impl From<Operand> for Expression {
    fn from(op: Operand) -> Self {
        Expression::Operand(op)
    }
}

impl From<Operand> for f64 {
    fn from(op: Operand) -> Self {
        match op {
            Operand::Rational(r) => r.into(),
            Operand::Irrational(r) => r.0,
        }
    }
}

impl From<f64> for Operand {
    fn from(oth: f64) -> Self {
        Operand::Irrational(oth.into())
    }
}

impl From<i64> for Operand {
    fn from(oth: i64) -> Self {
        Operand::Rational(oth.into())
    }
}

impl Operand {
    pub fn pow(self, oth: Operand) -> Operand {
        match self {
            Operand::Rational(mut a) => match oth {
                Operand::Rational(mut b) => {
                    use num_integer::Roots;
                    let mut flip = false;
                    if b < Rational::new(0, 0) {
                        flip = true;
                        b.abs();
                    }

                    let a_copy = a;
                    for _ in 0..(b.top - 1) {
                        a = a * a_copy;
                    }

                    a.top = a.top.nth_root(b.bot as u32);
                    a.bot = a.bot.nth_root(b.bot as u32);

                    if flip {
                        a.reciprocate();
                    }
                    a.into()
                }
                Operand::Irrational(b) => {
                    let a: f64 = a.into();
                    Operand::Irrational(a.powf(b.0).into())
                }
            },
            Operand::Irrational(a) => {
                let b: f64 = match oth {
                    Operand::Rational(b) => b.into(),
                    Operand::Irrational(b) => *b,
                };

                Operand::Irrational((*a).powf(b).into())
            }
        }
    }

    pub fn simplify(self) -> Self {
        match self {
            Operand::Rational(r) => Operand::Rational(r.simplify()),
            Operand::Irrational(r) => {
                if r.0.round() == r.0 {
                    Operand::Rational(Rational::new(r.0.round() as i64, 1)).simplify()
                } else {
                    self
                }
            }
        }
    }
}

impl ParseExpr for Operand {
    type Err = OperandError;
    fn parse(s: &str, fd: &FuncDict) -> std::result::Result<Expression, Self::Err> {
        let trimmed = s.trim();
        if trimmed.contains('.') {
            Ok(Irrational::parse(trimmed, fd)?)
        } else {
            Ok(Rational::parse(trimmed, fd)?)
        }
    }
}

impl Evaluatable for Operand {
    fn eval(&self, _: &FuncDict) -> Operand {
        self.simplify()
    }
}

macro_rules! impl_operator {
    { $op:path, $name:ident, $sym:tt } => {
        impl $op for Operand {
            type Output = Operand;
            fn $name(self, oth: Operand) -> Self::Output {
                match self {
                    Operand::Rational(a) => {
                        match oth {
                            Operand::Rational(b) => {
                                Operand::Rational(a $sym b)
                            },
                            Operand::Irrational(b) => {
                                Operand::Irrational(a $sym b)
                            }
                        }
                    },
                    Operand::Irrational(a) => {
                        match oth {
                            Operand::Rational(b) => {
                                (a $sym b).into()
                            },
                            Operand::Irrational(b) => {
                                let a: f64 = *a $sym *b;
                                let a: Irrational = a.into();
                                a.into()
                            }
                        }
                    }
                }
            }
        }
    }
}

impl_operator!(std::ops::Add, add, +);
impl_operator!(std::ops::Sub, sub, -);
impl_operator!(std::ops::Mul, mul, *);
impl_operator!(std::ops::Div, div, /);
impl_operator!(std::ops::Rem, rem, %);

#[derive(Debug, PartialEq, Eq, Error)]
pub enum OperandError {
    #[error(transparent)]
    RationalError(#[from] rational::RationalError),
    #[error(transparent)]
    IrrationalError(#[from] irrational::IrrationalError),
}

impl IsErrorUseful for OperandError {
    fn is_useful(&self) -> bool {
        true
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_simplify() {
        use super::operator_exp::*;

        let a: Operand = 0.5.into();
        let b: Operand = Rational::new(4, 1).into();

        let op_exp = OperatorExp {
            operand_a: Box::new(Expression::Operand(a)),
            operator: Operator::Multiply,
            operand_b: Box::new(Expression::Operand(b)),
        };

        assert_eq!(
            op_exp.eval(&FuncDict::new()),
            Operand::Rational(Rational::new(2, 1))
        );
    }

    #[test]
    fn test_pow() {
        let a: Operand = Rational::new(2, 1).into();
        let b: Operand = Rational::new(5, 1).into();

        assert_eq!(b.pow(a), Operand::Rational(Rational::new(25, 1)));

        let a: Operand = Rational::new(1, 3).into();
        let b: Operand = Rational::new(8, 1).into();

        assert_eq!(b.pow(a), Operand::Rational(Rational::new(2, 1)));

        let a: Operand = Rational::new(-1, 1).into();
        let b: Operand = Rational::new(2, 1).into();

        assert_eq!(b.pow(a), Operand::Rational(Rational::new(1, 2)));

        let a: Operand = Rational::new(17, 1).into();
        let b: Operand = Rational::new(4, 3).into();

        assert_eq!(
            b.pow(a),
            Operand::Rational(Rational::new(17179869184, 129140163))
        );

        let a: Operand = Rational::new(4, 1).into();
        let b: Operand = Irrational(2.0).into();

        assert_eq!(b.pow(a).simplify(), Operand::Rational(Rational::new(16, 1)));
    }
}
