use std::fmt::{Display, Formatter, Result as FmtResult};
use std::str::FromStr;

use thiserror::Error;

#[derive(Debug, Error)]
pub enum OperatorError {
    #[error("unknown operator '{0}'")]
    UnknownOperator(String),
}

type Result<T> = std::result::Result<T, OperatorError>;

#[derive(Clone, Debug, PartialEq, Eq, Copy)]
pub enum Operator {
    Plus,
    Minus,
    Divide,
    Multiply,
    Modulus,
    Power,
}

impl Display for Operator {
    fn fmt(&self, fmt: &mut Formatter) -> FmtResult {
        use Operator::*;

        write!(
            fmt,
            "{}",
            match self {
                Plus => "+",
                Minus => "-",
                Divide => "/",
                Multiply => "*",
                Modulus => "%",
                Power => "^",
            }
        )
    }
}

impl Operator {
    pub fn get_prec(&self) -> usize {
        use Operator::*;

        match self {
            Plus | Minus => 0,
            Multiply | Divide | Modulus => 1,
            Power => 2,
        }
    }
}

impl FromStr for Operator {
    type Err = OperatorError;
    fn from_str(s: &str) -> Result<Self> {
        use Operator::*;
        match s.trim() {
            "+" => Ok(Plus),
            "-" => Ok(Minus),
            "/" => Ok(Divide),
            "*" => Ok(Multiply),
            "%" => Ok(Modulus),
            "^" => Ok(Power),
            _ => Err(OperatorError::UnknownOperator(s.trim().into())),
        }
    }
}
