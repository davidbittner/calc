use super::*;
use regex::Regex;
use thiserror::Error;

lazy_static::lazy_static! {
    static ref OPERATOR_REG: Regex =
        Regex::new("(\\+|/|-|\\*|%|\\^)")
            .expect("operator reg");
}

///An expression with two operands and an operator
#[derive(Clone, PartialEq, Debug)]
pub struct OperatorExp {
    pub operand_a: Box<Expression>,
    pub operator: Operator,
    pub operand_b: Box<Expression>,
}

use std::fmt::{Display, Formatter, Result as FmtResult};
impl Display for OperatorExp {
    fn fmt(&self, fmt: &mut Formatter) -> FmtResult {
        use std::ops::Deref;

        match self.operand_a.deref() {
            Expression::Operator(exp) => write!(fmt, "({})", exp)?,
            Expression::Function(exp) => write!(fmt, "({})", exp)?,
            Expression::Operand(op) => write!(fmt, "{}", op)?,
        };

        write!(fmt, " {} ", self.operator,)?;

        match self.operand_b.deref() {
            Expression::Operator(exp) => write!(fmt, "({})", exp),
            Expression::Function(exp) => write!(fmt, "({})", exp),
            Expression::Operand(op) => write!(fmt, "{}", op),
        }
    }
}

#[derive(Debug, PartialEq, Eq, Error)]
pub enum OperatorExpError {
    #[error("failed to generate useful error")]
    Garbage,
    #[error(transparent)]
    ExpressionError(#[from] ExpressionParseError),
}

impl IsErrorUseful for OperatorExpError {
    fn is_useful(&self) -> bool {
        !matches!(self, Self::Garbage)
    }
}

fn count_depth(s: &str, st: usize) -> usize {
    let mut left = 0i32;
    let mut right = 0i32;

    for ch in s[0..st].chars() {
        if ch == '(' {
            left += 1;
        } else if ch == ')' {
            left -= 1;
        }
    }

    for ch in s[st..].chars() {
        if ch == '(' {
            right += 1;
        } else if ch == ')' {
            right -= 1;
        }
    }

    std::cmp::min(left.abs(), right.abs()) as usize
}

use crate::expressions::func::find_matching;

impl ParseExpr for OperatorExp {
    type Err = OperatorExpError;
    fn parse(s: &str, fd: &FuncDict) -> Result<Expression, Self::Err> {
        let mut s = s.trim();
        if let Some(loc) = find_matching(s, 0) {
            if loc == s.len() - 1 {
                s = &s[1..s.len() - 1];
            }
        }

        let mut oplocs: Vec<_> = OPERATOR_REG
            .find_iter(s)
            .map(|m| {
                let op_s = m.as_str();
                let op = op_s.parse::<Operator>().unwrap();
                (
                    m.start(),
                    op,
                    count_depth(s, m.start()) * 10 + op.get_prec(),
                )
            })
            .collect();

        if oplocs.is_empty() {
            return Err(Self::Err::Garbage);
        }

        let mut delete = Vec::new();
        //This is for determining if a operand is a negative value (or if there is a subtraction
        //operator)
        {
            let mut peeking_iter = oplocs.iter().enumerate().peekable();
            //Since we already know we're looking through operators, we only care about whether or
            //not they are next to eachother
            while let Some((_, (loc, _, _))) = peeking_iter.next() {
                if let Some((n_ind, (n_loc, _, _))) = peeking_iter.peek() {
                    if *n_loc == (loc + 2) {
                        delete.push(*n_ind);
                    }
                }
            }
        }

        //Now that we know where those operands are, we can choose to delete them (as they are
        //instead used to signal negative values)
        for del_ind in delete.into_iter().rev() {
            oplocs.remove(del_ind);
        }

        oplocs.sort_by(|(a_st, _, a_p), (b_st, _, b_p)| match b_p.cmp(a_p) {
            std::cmp::Ordering::Equal => a_st.cmp(b_st),
            _ => b_p.cmp(a_p),
        });

        let (st, op, _) = oplocs.pop().ok_or(OperatorExpError::Garbage)?;

        let a = Expression::parse(&s[0..st], fd)?;
        let b = Expression::parse(&s[st + 1..], fd)?;

        Ok(Expression::Operator(Self {
            operand_a: a.into(),
            operand_b: b.into(),
            operator: op,
        }))
    }
}

impl Evaluatable for OperatorExp {
    fn eval(&self, fd: &FuncDict) -> Operand {
        use self::Operator::*;
        let a = self.operand_a.eval(fd).simplify();
        let b = self.operand_b.eval(fd).simplify();

        match self.operator {
            Plus => a + b,
            Minus => a - b,
            Divide => a / b,
            Multiply => a * b,
            Modulus => a % b,
            Power => a.pow(b),
        }
        .simplify()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse() {
        let test = OperatorExp::parse("( 2 + 2)* 2/3", &FuncDict::new()).unwrap();

        assert_eq!(
            Operand::Rational(Rational::new(8, 3)),
            test.eval(&FuncDict::new())
        );

        let test = OperatorExp::parse(
            "((((100_2 - 2) / 4) + 17 * (4-2) / 14) * 6) + 2",
            &FuncDict::new(),
        )
        .unwrap();

        assert_eq!(
            Operand::Rational(Rational::new(137, 7)),
            test.eval(&FuncDict::new())
        );
    }

    #[test]
    fn test_parse_negative() {
        let test = OperatorExp::parse("2 + -5", &FuncDict::new()).unwrap();

        assert_eq!(
            Operand::Rational(Rational::new(-3, 1)),
            test.eval(&FuncDict::new())
        );

        let test = OperatorExp::parse(
            "((((100_2 - 2) / 4) + 17 * (4-2) / 14) * -6) + 2",
            &FuncDict::new(),
        )
        .unwrap();

        assert_eq!(
            Operand::Rational(Rational::new(-109, 7)),
            test.eval(&FuncDict::new())
        );

        let test = OperatorExp::parse("100_4 * 22 / -5 + -3", &FuncDict::new()).unwrap();

        assert_eq!(
            Operand::Rational(Rational::new(-367, 5)),
            test.eval(&FuncDict::new())
        );
    }

    #[test]
    fn parse_w_func() {
        use std::sync::Arc;

        let dict = {
            let mut temp = FuncDict::new();
            temp.insert(
                "pi".into(),
                Arc::new(|_| Operand::Irrational(std::f64::consts::PI.into())),
            );

            temp
        };

        let test = OperatorExp::parse("(pi() * pi())/pi() / 2.0 * (pi() / pi() + 1) - pi()", &dict)
            .unwrap();

        assert_eq!(
            Operand::Irrational(0.0.into()).simplify(),
            test.eval(&dict).simplify()
        );
    }

    #[test]
    fn find_matching_func() {
        let test = "(1234567)";
        assert_eq!(Some(8), find_matching(test, 0));

        let test = "12((()45678))";
        assert_eq!(Some(12), find_matching(test, 2));
    }
}
