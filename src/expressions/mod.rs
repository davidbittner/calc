pub mod func;
pub mod operand;
pub mod operator;
pub mod operator_exp;

pub use func::*;
pub use operand::*;
pub use operator::*;
pub use operator_exp::*;

pub use crate::prelude::*;

use thiserror::Error;

pub trait IsErrorUseful {
    fn is_useful(&self) -> bool;
}

#[derive(Debug, PartialEq, Clone)]
pub enum Expression {
    Function(FuncExp),
    Operator(OperatorExp),
    Operand(Operand),
}

use std::fmt::{Display, Formatter, Result as FmtResult};
impl Display for Expression {
    fn fmt(&self, fmt: &mut Formatter) -> FmtResult {
        match self {
            Expression::Function(f) => write!(fmt, "{}", f),
            Expression::Operator(o) => write!(fmt, "{}", o),
            Expression::Operand(o) => write!(fmt, "{}", o),
        }
    }
}

#[derive(Debug, PartialEq, Eq, Error)]
pub enum ExpressionParseError {
    #[error("failed parsing '{context}':\n\t{error}")]
    ExpectedOperand {
        context: String,
        error: OperandError,
    },
    #[error("{error}")]
    ExpectedOperator { error: Box<OperatorExpError> },
    #[error("failed parsing '{context}':\n\t{error}")]
    ExpectedFunction {
        context: String,
        error: FuncParseError,
    },
    #[error("failed parsing '{context}'")]
    Unknown { context: String },
}

impl ParseExpr for Expression {
    type Err = ExpressionParseError;

    fn parse(s: &str, fd: &FuncDict) -> Result<Self, Self::Err> {
        let s = s.trim();

        let operand_res = Operand::parse(s, fd);
        if let Ok(res) = operand_res {
            return Ok(res);
        }

        let operator_exp_res = OperatorExp::parse(s, fd);
        if let Ok(res) = operator_exp_res {
            return Ok(res);
        }

        let func_exp_res = FuncExp::parse(s, fd);
        if let Ok(res) = func_exp_res {
            return Ok(res);
        }

        let operand_err = operand_res.unwrap_err();
        let func_err = func_exp_res.unwrap_err();
        let operator_exp_err = operator_exp_res.unwrap_err();

        let useful = (
            operand_err.is_useful(),
            func_err.is_useful(),
            operator_exp_err.is_useful(),
        );

        match useful {
            (_, true, false) => Err(ExpressionParseError::ExpectedFunction {
                context: s.to_string(),
                error: func_err,
            }),
            (_, false, true) => Err(ExpressionParseError::ExpectedOperand {
                context: s.to_string(),
                error: operand_err,
            }),
            (true, false, false) => Err(ExpressionParseError::ExpectedOperand {
                context: s.to_string(),
                error: operand_err,
            }),
            _ => Err(ExpressionParseError::Unknown {
                context: s.to_string(),
            }),
        }
    }
}

impl Evaluatable for Expression {
    fn eval(&self, fd: &FuncDict) -> Operand {
        use Expression::*;

        match self {
            Operator(exp) => exp.eval(fd).simplify(),
            Operand(exp) => exp.eval(fd).simplify(),
            Function(exp) => exp.eval(fd).simplify(),
        }
    }
}
