//! # Purpose
//!
//! This project aims to be a mathematical expression parser that I plan to use in a command line calculator.
//!
//! # Usage
//!
//! This project exposes a structure called an `ExpressionBuilder`.
//! This lets you parse a mathematical expression, as well as give it a `HashMap<String, fn(Vec<Operand>) -> Operand`.
//! This type is aliased to `FuncDict`. This HashMap allows you to define functions that can be called within the expressions.
//!
//! For example, if you created a FuncDict that looked like this:
//! ``` rust
//! # fn main() {
//! # use std::sync::Arc;
//! use calc::prelude::*;
//!
//! let mut fd = FuncDict::new();
//! let func = |params: Vec<Operand>| {
//!     params
//!         .into_iter()
//!         .fold(Rational::new(0, 0).into(), |tot, v| tot + v)
//! };
//! fd.insert(
//!     "add".into(),
//!     Arc::new(func)
//! );
//!
//! //By writing a number as {NUM}_{RADIX}, you can easily
//! //define numbers in another base. 30_4 is equal to 12.
//! let expr = "add(5, 2, 30_4, 17)";
//! let expr = ExpressionBuilder::from(expr)
//!     .with_functions(fd)
//!     .build()
//!     .expect("failed to parse expression");
//!
//! assert_eq!(Operand::Rational(Rational::new(36, 1)), expr.eval())
//! # }
//! ```
//!
//! # Features
//!
//! + Parsing hex, and any other base with the format `NUM_Radix`. For example, `1001_2` would result in 9.
//! + User defined functions, you could interface this with Lua as well allowing the user to define functions in a script that can be called in the expressions.

pub mod expressions;
pub mod prelude;
use expressions::*;
use prelude::*;

use std::fmt::{Display, Formatter, Result as FmtResult};

pub struct RootExpression {
    root: Expression,
    fd: FuncDict,
}

impl Display for RootExpression {
    fn fmt(&self, fmt: &mut Formatter) -> FmtResult {
        write!(fmt, "{}", self.root)
    }
}

pub struct ExpressionBuilder<'a> {
    expr: &'a str,
    fd: Option<FuncDict>,
}

impl RootExpression {
    pub fn eval(&self) -> Operand {
        self.root.eval(&self.fd)
    }
}

impl<'a> From<&'a str> for ExpressionBuilder<'a> {
    fn from(s: &'a str) -> Self {
        Self { expr: s, fd: None }
    }
}

impl<'a> ExpressionBuilder<'a> {
    pub fn with_functions(self, fd: FuncDict) -> Self {
        Self {
            expr: self.expr,
            fd: Some(fd),
        }
    }

    pub fn with_stdlib(self) -> Self {
        Self {
            expr: self.expr,
            fd: Some(prelude::STD_LIB.clone()),
        }
    }

    pub fn build(self) -> Result<RootExpression, ExpressionParseError> {
        let fd = self.fd.unwrap_or_default();
        Ok(RootExpression {
            root: Expression::parse(self.expr, &fd)?,
            fd,
        })
    }
}

#[cfg(test)]
mod root_tests {
    use super::*;

    #[test]
    fn builder() {
        let expr = "(2 * 4) / 10010_2";
        let expr = ExpressionBuilder::from(expr).build();

        assert!(expr.is_ok());
        let expr = expr.unwrap();
        assert_eq!(
            Operand::Rational(Rational::new(4, 9)),
            expr.eval().simplify()
        );
    }

    #[test]
    fn format() {
        let expr = "((((100_2 - 2) / 4) + 17 * (4-2) / 14) * 6) + 2";
        let expr = ExpressionBuilder::from(expr).build().unwrap();

        let ts_exp = expr.to_string();

        let oth_exp = ExpressionBuilder::from(ts_exp.as_str()).build().unwrap();

        assert_eq!(expr.eval(), oth_exp.eval());
    }
}
