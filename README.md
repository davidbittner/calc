 # Purpose

 This project aims to be a mathematical expression parser that I plan to use in a command line calculator.
 
 # Usage
 
 This project exposes a structure called an `ExpressionBuilder`.
 This lets you parse a mathematical expression, as well as give it a `HashMap<String, fn(Vec<Operand>) -> Operand`.
 This type is aliased to `FuncDict`. This HashMap allows you to define functions that can be called within the expressions.
 
 For example, if you created a FuncDict that looked like this:
 ``` rust
 use calc::prelude::*;

 let mut fd = FuncDict::new();
 let func = |params: Vec<Operand>| {
     params
         .into_iter()
         .fold(Rational::new(0, 0).into(), |tot, v| tot + v)
 };
 fd.insert(
     "add".into(), 
     Rc::new(func)
 );

 let expr = "add(5, 2, 30_4, 17)";
 let expr = ExpressionBuilder::from(expr)
     .with_functions(fd)
     .build()
     .expect("failed to parse expression");

 assert_eq!(Operand::Rational(Rational::new(27, 1)), expr.eval())
 ```
 
 # Features
 
 + Parsing hex, and any other base with the format `NUM_Radix`. For example, `1001_2` would result in 9.
 + User defined functions, you could interface this with Lua as well allowing the user to define functions in a script that can be called in the expressions.
